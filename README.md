# What does this project do

This is an app for automatically adding synonyms to a wanikani library. The synonyms will be equal to the first letters of all words for given radical/kanji/word, for example: for a vocabulary piece meaning "two things", a synonym "tt" will be added.

# How to use

1. `cp .env.example .env`
2. Add your Wanikani API key after `=` in the `.env` file
1. `bundle install`
2. Run `bin/app`

The update process will take a few hours. This is due to Wanikani's vocabulary volume & [API rate limit](https://docs.api.wanikani.com/20170710/#rate-limit).

# Development

To help with development, a console command is available under `bin/console` command.

