class AddSynonyms
  def process
    response = api.get_subjects
    handle_response_page(response)
  end

  private

  def api
    @_api ||= WanikaniApi.new
  end

  def handle_response_page(response)
    response['data'].each do |subject|
      subject_id = subject['id']
      meanings = load_meanings(subject)
      material = load_material(subject_id)

      add_missing_abbreviations(meanings, material, subject_id)
    end

    handle_next_page(response['pages']['next_url'])
  end

  def load_meanings(subject)
    subject['data']['meanings'].map do |meaning|
      meaning['meaning']
    end
  end

  def load_material(subject_id)
    materials = api.get_study_materials(subject_id)

    raise StandardError, materials if materials['data'].count > 1

    materials['data'].first
  end

  def add_missing_abbreviations(meanings, material, subject_id)
    missing_abbreviations = []
    existing_abbreviations = material ? material['data']['meaning_synonyms'] : []
    meanings.each do |meaning|
      abbreviation = meaning.split.map(&:chr).join.downcase
      next if existing_abbreviations.include?(abbreviation)

      missing_abbreviations << abbreviation
    end

    if missing_abbreviations.count > 0
      puts "-------------------"
      puts "NEW MAT ALERT #{subject_id}"
      puts "meanings #{meanings}"
      puts "material #{existing_abbreviations}"
      puts "missing_abbreviations #{missing_abbreviations}"
      abbreviations = (existing_abbreviations + missing_abbreviations).uniq.first(WanikaniApi::MAX_SYNONYMS)
      api_add_abbreviations(subject_id, abbreviations, material)
    end
  end

  def api_add_abbreviations(subject_id, abbreviations, material)
    if material
      puts "UPDATE"
      update_material(subject_id, abbreviations, material)
    else
      puts "CREATE"
      create_material(subject_id, abbreviations)
    end
  end

  def update_material(subject_id, abbreviations, material)
    payload = {
      study_material: {
        subject_id: subject_id,
        meaning_synonyms: abbreviations
      }
    }
    api.put_study_material(material['id'], payload)
  end

  def create_material(subject_id, abbreviations)
    payload = {
      study_material: {
        subject_id: subject_id,
        meaning_synonyms: abbreviations
      }
    }
    api.post_study_material(payload)
  end

  def handle_next_page(next_url)
    return unless next_url

    response = api.get(next_url)
    handle_response_page(response)
  end
end
