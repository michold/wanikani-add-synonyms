# frozen_string_literal: true

class WanikaniApi
  API_BASE_URL = "https://api.wanikani.com/v2/"
  MAX_SYNONYMS = 8

  def initialize
    @api_key = ENV['WANIKANI_API_KEY']

    raise ArgumentError, "WANIKANI_API_KEY ENV variable needs to be set" unless api_key
  end

  def get_subjects
    get("#{API_BASE_URL}subjects")
  end

  def get_study_materials(subject_id)
    get("#{API_BASE_URL}study_materials?subject_ids=#{subject_id}")
  end

  def put_study_material(study_material_id, payload)
    put("#{API_BASE_URL}study_materials/#{study_material_id}", payload)
  end

  def post_study_material(payload)
    post("#{API_BASE_URL}study_materials", payload)
  end

  def get(url)
    response = RestClient.get(url, headers)
    json(response)
  rescue RestClient::TooManyRequests => err
    handle_get_err(err, url)
  end

  private

  attr_reader :api_key

  def post(url, payload)
    response = RestClient.post(url, payload, headers)
    json(response)
  rescue RestClient::TooManyRequests => err
    handle_post_err(err, url, payload)
  end

  def put(url, payload)
    response = RestClient.put(url, payload, headers)
    json(response)
  rescue RestClient::TooManyRequests => err
    handle_put_err(err, url, payload)
  end

  def headers
    @_headers ||= {
      authorization: "Bearer #{api_key}"
    }
  end

  def post_headers
    @_post_headers ||= headers.merge(
      content_type: :json,
      accept: :json
    )
  end

  def json(response)
    JSON.parse(response.body)
  end

  def handle_get_err(err, url)
    reset_time = Time.at(err.http_headers[:ratelimit_reset].to_i)
    sleep_time = reset_time - Time.now
    sleep(sleep_time) if sleep_time > 0

    get(url)
  end

  def handle_post_err(err, url, payload)
    reset_time = Time.at(err.http_headers[:ratelimit_reset].to_i)
    sleep_time = reset_time - Time.now
    sleep(sleep_time) if sleep_time > 0

    post(url, payload)
  end

  def handle_put_err(err, url, payload)
    reset_time = Time.at(err.http_headers[:ratelimit_reset].to_i)
    sleep_time = reset_time - Time.now
    sleep(sleep_time) if sleep_time > 0

    put(url, payload)
  end
end
